# Modifer une image de la HomePage ou l'agenda

Il faut avoir un compte sur GitLab pour réaliser cette opération.

- Se connecter à GitLab, et aller dans le projet  **wood-site**
- Retrouver les éléments statiques: wood -> wood-core -> static/wood-core
- Les fichiers qui nous intéressent se trouvent dans  *files* (l'agenda par ex.), et dans  *images* (pour les images de la home-page)

- Aller dans le dossier approprié en fonction du fichier à modifier et cliquer sur le fichier en question.
- Cliquer sur le bouton en haut à droite "REPLACE" et glisser le fichier de remplacement dans le cadre affiché. **Attention!** Le nouveau fichier doit avoir exactement le même nom que le fichier précédent! 
- Ne pas modifier le "commit message". Cliquer sur REPLACE FILE pour confirmer.

- Pour que ces changements soient mis en ligne: envoyer un message à Nina ou à Pierre M. de l'Atelier Carto, pour qu'ils lancent la commande de mise à jour du site (ça nous prend une seconde et il nous semble que cela ne vaut pas la peine d'installer une mise à jour automatique pour l'instant)


## Remarques: 
- Pour le portrait de l'arbre du mois, il faut remplacer deux fichier (une photo en mode paysage: "portrait-mod.jpg", l'autre en mode portrait: "portrait.jpg")
- Pour les images de la Home Page, il faut idéalement que la photo soit assez sombre et unie dans le bas inférieur gauche, afin que le texte soit lisible. 
- La taille du portrait du mois en paysage est actuellement: 3959 x 2009 (en px). Cette taille peut légèrement varier, mais la proportion Longueur/Largeur doit se situer entre 1.5 et 2. Elle sera ensuite rognée pour s'adapter aux écrans.
- La taille du portrait du mois en portrait est actuellement : 1684 x 2788 (en px). De même, cette taille peut varier, mais doit idéalement rester dans une proportion Hauteur/Largeur située entre 1.4 et 1.6. 


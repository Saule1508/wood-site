from django.urls import path, include

from .views import (
    create_record,
    update_record,
)


urlpatterns = [
    path('add/<message_type>/', create_record, name='lingua-create-record'),
    path('update/<message_type>/<id>', update_record, name='lingua-update-record'),
]

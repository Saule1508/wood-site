from django.apps import AppConfig


class WoodCoreConfig(AppConfig):
    name = 'wood_core'

    def ready(self):
        from .signals import connect
        connect()

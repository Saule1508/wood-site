from django.shortcuts import render

def walks_1050(request, lang):
    """Link to Ixelles walks and other activities
    """

    template_name = 'wood_core/index-1050.html'
    context = {
        'lang':lang
    }

    return render(request, template_name, context)
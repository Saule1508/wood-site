from django.shortcuts import render, get_object_or_404
from django.http import (
    JsonResponse,
    HttpResponseForbidden,
)
from django.urls import reverse


from wood_core.models.subject import Tree
from wood_core.models.ancillary import Portrait
from wood_core.models.contrib import TextContribution, ImageContribution

from wood_walk.models import  WalkEdge

from django.core.cache import cache

from .common import *


def atlas_map(request, lang):
    """Serve the atlas map
    """
    template_name = 'wood_core/site-{}/map.html'.format(lang)
    context = {'lang': lang}
    return render(request, template_name, context)


def focus(request, lang, tid):
    """Highlights for a subject tree
    """
    tree = get_object_or_404(Tree, id=tid)
    data = [h.to_dict(lang) for h in tree.get_highlights()]

    return JsonResponse(dict(focus=data))


class GeoAcc:
    def __init__(self):
        self.features = []

    def push(self, coords, props):
        x, y = transformer.transform(coords[1], coords[0])
        # Sometimes; somewhere; we ended up with points encoded in L72, ...
        if coords[0] > 180 or coords[0] < -180:
            x, y = coords
        self.features.append(
            dict(
                type='Feature',
                geometry=dict(type="Point", coordinates=[x, y]),
                properties=props))

    def geojson(self):
        return dict(type="FeatureCollection", features=self.features)


def get_path_a(obj, p, dflt):
    lp = len(p)
    if lp > 0:
        comp = p[0]
        try:
            new_obj = getattr(obj, comp)
            if lp == 1:
                return new_obj
            else:
                return get_path_a(new_obj, p[1:], dflt)
        except Exception:
            return dflt
    return dflt


def get_path(obj, p, dflt=None):
    return get_path_a(obj, p.split('.'), dflt)


def edgetoWalk(edge):
    return (edge.walk_id)


def walktoURL(lang):
    def inner(walk_id):
        return reverse('wood-walk-single', args=[lang, walk_id])
    return inner


def edgestoURLS(lang, edges):
    return list(map(walktoURL(lang), (map(edgetoWalk, edges))))


def cache_trees(key, geo_acc):
    """ get the list with the key "key" from the cache and put it in a dict "geo_acc" 
    """
    in_cache = cache.get(key)
    for coords, props in in_cache:
        geo_acc.push(coords, props)
    return geo_acc


def import_trees(key, geo_acc, user):
    """ import a list of trees (keys: tree, contrib or item) put it in a dict "geo_acc"
    """
    to_cache = []
    qs = Tree.objects.filter(category__isnull=False,
                             taxon__isnull=False, validated=True)
    qs.select_related(
        'name',
        'taxon__name',
        'taxon__trivia',
        'street_address',
    )

    all_edges = list(WalkEdge.objects.all())

    validated_edges = []
    if user.is_authenticated and user.is_superuser:
        validated_edges = all_edges
    else:
        for e in all_edges:
            if e.walk.validated:
                validated_edges.append(e)

    all_portraits = list(Portrait.objects.all())


    for tree in qs:

        tid = tree.id
        # Record the next first trees related to this tree (only one if there is only one walk through this tree)
        # edges = list(tree.walk_in_node.all()) + list(tree.walk_out_node.all())
        edges = list(filter(lambda e: e.in_node_id ==
                            tid or e.out_node_id == tid, validated_edges))
        portraits = list(filter(lambda e: e.tree_id == tid, all_portraits))
        nextTreesXY = []
        for e in edges:
            nextTreesXY.append(e.out_node.position.coords)

        props = dict(
            id=tree.id,
            category=tree.category,
            has_portrait=len(portraits) > 0,
            # has_portrait=tree.portrait_set.count() > 0,
            nom_fr=get_path(tree, 'name.fr', get_path(tree, 'taxon.name.fr')),
            nom_nl=get_path(tree, 'name.nl', get_path(tree, 'taxon.name.nl')),
            nom_en=get_path(tree, 'name.en', get_path(tree, 'taxon.name.en')),
            famille_fr=get_path(tree, 'taxon.trivia.fr'),
            famille_nl=get_path(tree, 'taxon.trivia.nl'),
            famille_en=get_path(tree, 'taxon.trivia.en'),
            adresse_1=get_path(tree, 'street_address.fr'),
            adres_1=get_path(tree, 'street_address.nl'),
            latin='{} {}'.format(tree.taxon.genus, tree.taxon.species),
            wood_file='atlas/{}.html'.format(tree.id),
            highlight=tree.is_highlighted(),
            walks_fr=(edgestoURLS('fr', edges),
                      tree.position.coords, nextTreesXY),
            walks_nl=(edgestoURLS('nl', edges),
                      tree.position.coords, nextTreesXY),
            walks_en=(edgestoURLS('en', edges),
                      tree.position.coords, nextTreesXY),
        )
        to_cache.append((tree.position.coords, props))
        geo_acc.push(tree.position.coords, props)

    return (to_cache, geo_acc)


def atlas_data(request):
    """Serve the atlas data
    """
    geo_acc = GeoAcc()
    u = request.user
    tree_cache = True

    # if we have datas in cache:
    if tree_cache and cache.get('tree'):
        print('CACHE')
        geo_acc = cache_trees('tree', geo_acc)
    # if cache empty:
    else:
        print('NO CACHE')
        to_cache, geo_acc = import_trees('tree', geo_acc, u)
        cache.set('tree', to_cache, 60*60*24)

    return JsonResponse(geo_acc.geojson())


MEDIA_SIZES = (2000, 1800, 1600, 1400, 1200, 1000, 800, 600, 400)


def atlas_page(request, lang, pid):
    """Serve an identification page for a tree
    """
    obj = get_object_or_404(Tree, pk=pid)
    textcontribs = TextContribution.objects.filter(tree_id=pid, validated=True)
    imgcontribs = ImageContribution.objects.filter(
        tree_id=pid, validated=True).order_by('id')

    contributors = []
    if obj.contributor:
        contributors.append(obj.contributor)
    for c in textcontribs:
        if c.contributor not in contributors:
            contributors.append(c.contributor)
    for c in imgcontribs:
        if c.contributor not in contributors:
            contributors.append(c.contributor)

    if obj.category is None:
        return HttpResponseForbidden('for reasons')

    # template_name = 'wood_core/fiche-{}.html'.format(obj.category)
    template_name = 'wood_core/fiche.html'
    context = {
        'tree': obj,
        'textcontribs': textcontribs,
        'imgcontribs': imgcontribs,
        # 'firstImage': firstImg,
        'contributors': contributors,
        'lang': lang,
        'media_sizes': MEDIA_SIZES,
    }
    return render(
        request,
        template_name,
        context,
    )


def atlas_pdf(request):
    """Serve an identification document (PDF) for a tree
    """
    pass

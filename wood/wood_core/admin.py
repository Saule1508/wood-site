from django.contrib.gis.admin import GeoModelAdmin
from django.contrib.admin import site, ModelAdmin, SimpleListFilter, StackedInline
from easy_thumbnails.widgets import ImageClearableFileInput
from easy_thumbnails.fields import ThumbnailerImageField

from .models.subject import (
    Tree,
    TreePicture,
    Jumelage,
    LinkedMedia,
    Remarqueur,
    Activity,
)
from .models.ancillary import Portrait
from .models.media import EmbedMedia
from .models.contrib import (
    Contribution,
    PictureContrib,
    ImageContribution,
    TextContribution,
)
from .models.highlight import Highlight
from .models.service import TreeQuality, TreeService


class GisModelAdmin(GeoModelAdmin):
    map_template = "wood_core/admin/gis.html"
    wms_url = "https://geoservices-urbis.irisnet.be/geoserver/urbisgrid/ows"
    wms_name = "Urbis Ortho2017"
    wms_layer = "Ortho2017"
    wms_options = {
        "VERSION": "1.1.1",
    }
    map_srid = 31370
    display_srid = 31370
    max_extent = "14697.30,22635.80,291071.84,246456.18"
    num_zoom = 20
    max_zoom = 20
    point_zoom = num_zoom - 6
    units = "m"


class TreePictureWidget(ImageClearableFileInput):
    def render(self, name, value, attrs=None, renderer=None):
        return super().render(name, value, attrs)


class TreePictureInline(StackedInline):
    formfield_overrides = {ThumbnailerImageField: dict(widget=TreePictureWidget)}
    model = TreePicture
    autocomplete_fields = search_fields = ("caption",)


class TextContribInline(StackedInline):
    model = TextContribution
    autocomplete_fields = search_fields = ("tree_id",)


class ImageContribInline(StackedInline):
    model = ImageContribution
    autocomplete_fields = search_fields = ("tree_id",)


class TreeAdmin(GisModelAdmin):
    list_display = (
        "id",
        "taxon",
        "name",
        "contributor",
        "validated",
        "category",
        "city",
        "street_address",
        "date",
    )
    list_editable = (
        "category",
        "validated",
    )
    list_filter = (
        "category",
        "city__fr",
    )
    inlines = (
        TextContribInline,
        ImageContribInline,
        TreePictureInline,
    )
    readonly_fields = ("import_id",)

    autocomplete_fields = (
        "height",
        "circumference",
        "crown_diameter",
        "name",
        "taxon",
        "description",
        "street_address",
        "city",
        "post_code",
    )

    search_fields = (
        "id",
        "name__fr",
        "name__nl",
        "name__en",
        "taxon__name__fr",
        "taxon__name__nl",
        "taxon__name__en",
    )


def image_name(instance):
    return instance.file.name


class PictureAdmin(ModelAdmin):
    list_display = (image_name, "linked_object", "caption", "sort_value")
    autocomplete_fields = search_fields = ("caption",)


class PortraitAdmin(ModelAdmin):
    list_display = ("tree", "monthly")


class LinkAdmin(ModelAdmin):
    list_display = ("tree", "label", "url")


class JumelageAdmin(ModelAdmin):
    list_display = ("tree", "scope", "label", "url")


class EmbedAdmin(ModelAdmin):
    list_display = ("tree", "provider", "media_id")


class ContribPictureWidget(ImageClearableFileInput):
    def render(self, name, value, attrs=None, renderer=None):
        return super().render(name, value, attrs)


class ContribPictureInline(StackedInline):
    formfield_overrides = {ThumbnailerImageField: dict(widget=ContribPictureWidget)}
    model = PictureContrib
    autocomplete_fields = search_fields = ("caption",)


class TextContribAdmin(GisModelAdmin):
    list_display = ("id", "tree", "contributor", "updated_at", "validated", "lang")
    list_editable = ("validated",)
    fields = ("tree", "contributor", "validated", "lang", "text")


class ImgContribAdmin(GisModelAdmin):
    list_display = ("tree", "contributor", "updated_at", "validated", "lang")
    list_editable = ("validated",)
    fields = ("tree", "contributor", "validated", "lang", "picture")

    formfieloverrides = {ThumbnailerImageField: dict(widget=TreePictureWidget)}


# class ContribAdmin(GisModelAdmin):
#     formfield_overrides = {
#         ThumbnailerImageField: dict(widget=TreePictureWidget)
#     }
#     list_display = ('contributor', 'taxon', 'updated_at', 'validated', 'lang')
#     list_editable = ('validated', )
#     inlines = (ContribPictureInline, )


class HighlightAdmin(ModelAdmin):
    list_display = ("note", "tree", "start", "end")


class TreeQualityAdmin(ModelAdmin):
    search_fields = (
        "value__fr",
        "value__en",
        "value__nl",
    )


class TreeServiceAdmin(ModelAdmin):
    list_display = (
        "__str__",
        "beauty",
        "biodiversity",
        # 'oxygeny',
        # 'purify',
        # 'filtery',
        # 'floody',
        # 'carbony',
        # 'sweety',
        # 'soily',
        # 'healthy',
    )
    autocomplete_fields = search_fields = (
        "beauty",
        "biodiversity",
        "oxygeny",
        "purify",
        "filtery",
        "floody",
        "carbony",
        "sweety",
        "soily",
        "healthy",
    )


site.register(Tree, TreeAdmin)
site.register(TreePicture, PictureAdmin)
site.register(Remarqueur, LinkAdmin)
site.register(Jumelage, JumelageAdmin)
site.register(LinkedMedia, LinkAdmin)
site.register(Activity, LinkAdmin)
site.register(Portrait, PortraitAdmin)
site.register(EmbedMedia, EmbedAdmin)
site.register(Highlight, HighlightAdmin)
# site.register(Contribution, ContribAdmin)
site.register(TreeQuality, TreeQualityAdmin)
site.register(TreeService, TreeServiceAdmin)
site.register(ImageContribution, ImgContribAdmin)
site.register(TextContribution, TextContribAdmin)

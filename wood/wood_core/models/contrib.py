#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import unicodedata
import re
from django.contrib.gis.db import models
from django.conf import settings
from easy_thumbnails.fields import ThumbnailerImageField
from datetime import datetime

from wood_core.fields import PointField
from wood_core.models.subject import Tree
from .media import picture


def slugify(s):
    slug = unicodedata.normalize('NFKD', s)
    slug = slug.lower()
    slug = re.sub(r'[^a-z0-9]+', '_', slug).strip('_')
    slug = re.sub(r'[_]+', '_', slug)
    return slug


def upload_to(instance, filename):
    return 'contrib/{0}/{1}'.format(instance.contributor.id, slugify(filename))


class Contribution(models.Model):
    """Historicaly models a Contribution, now used for ImageContribution and TextContribution
    """
    class Meta:
        ordering = ('-updated_at', )

    id = models.AutoField(primary_key=True)
    tree = models.ForeignKey(
        Tree, on_delete=models.CASCADE, related_name='contributions')
    lang = models.CharField(max_length=2)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    validated = models.BooleanField(default=True)
    contributor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    # taxon and position not useful anymore
    # taxon = models.ForeignKey(
    #     'wood_botanique.Taxon',
    #     related_name='contrib_taxon',
    #     on_delete=models.CASCADE,
    #     null=True,
    #     blank=True,
    # )
    # position = PointField(srid=4326, null=True, blank=True)

    # 'description' and 'image' come only in the image and text contributions now. This two not useful anymore.
    description = models.TextField(null=True, blank=True)
    image = ThumbnailerImageField(upload_to=upload_to, null=True, blank=True)

    def __str__(self):
        return '{}’s {}'.format(self.contributor, self.tree.taxon)

    def format_created_date(self):
        date = self.created_at
        return date.strftime("%d-%m-%Y")


class ImageContribution(Contribution):
    """Models an Image Contribution 
    """
    picture = ThumbnailerImageField(upload_to=upload_to)


class TextContribution(Contribution):
    """Models an Text Contribution 
    """
    text = models.TextField()

    def to_dict(self):
        return {
            "created": self.created_at.isoformat(),
            "updated": self.updated_at.isoformat(),
            "description": self.text,
        }


PictureContrib = picture(Contribution)

# Not used until we want to send different mails for validated/not validated contributions
class ContribLog(models.Model):
    CREATED = 'c'
    VALIDATED = 'v'
    EVENTS = (
        (CREATED, 'Created'),
        (VALIDATED, 'Validated'),
    )

    id = models.AutoField(primary_key=True)
    ts = models.DateTimeField(auto_now_add=True)
    contrib = models.ForeignKey(Contribution, on_delete=models.CASCADE)
    event = models.CharField(max_length=1, choices=EVENTS)

    @staticmethod
    def find_logs_for(cid):
        return ContribLog.objects.filter(contrib__id=cid)

class TreeLog(models.Model):
    CREATED = 'c'
    VALIDATED = 'v'
    EVENTS = (
        (CREATED, 'Created'),
        (VALIDATED, 'Validated'),
    )

    id = models.AutoField(primary_key=True)
    ts = models.DateTimeField(auto_now_add=True)
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE)
    event = models.CharField(max_length=1, choices=EVENTS)

    @staticmethod
    def find_logs_for(id):
        return TreeLog.objects.filter(tree__id=id)

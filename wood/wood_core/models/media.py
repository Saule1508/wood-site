#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import unicodedata
import re
from django.contrib.gis.db import models
from django.utils.safestring import mark_safe
from easy_thumbnails.fields import ThumbnailerImageField

from wood_lingua.models import message_field


def slugify(s):
    slug = unicodedata.normalize('NFKD', s)
    slug = slug.lower()
    slug = re.sub(r'[^a-z0-9]+', '_', slug).strip('_')
    slug = re.sub(r'[_]+', '_', slug)
    return slug


class UnboundImage(Exception):
    pass


def get_model_name(instance):
    T = type(instance)
    meta = T._meta
    return '{}.{}'.format(meta.app_label, meta.model_name)


PICTURE_LINK_FIELD = 'linked_object'


def upload_to(instance, fn):
    try:
        obj = getattr(instance, PICTURE_LINK_FIELD)
        if not isinstance(obj, models.Model):
            raise Exception('Not A Model')
        model_name = get_model_name(obj)
        id = obj.id
        return '{}/{}/{}'.format(model_name, id, slugify(fn))

    except Exception as ex:
        raise UnboundImage(
            'An image can not not belong to an object.\n{}'.format(ex))


class BasePicture(models.Model):
    class Meta:
        abstract = True

    id = models.AutoField(primary_key=True)

    file = ThumbnailerImageField(upload_to=upload_to)

    def __str__(self):
        return '{}'.format(self.file)

    # def tag(self):
    #     thumb_url = self.file.admin.url
    #     return mark_safe('<img src="{}" title="{}" />'.format(
    #         thumb_url, self.file.name))

    # tag.short_description = 'Image'


picture_models = dict()


def picture(model, **fields):
    meta = model._meta
    is_sorted = fields.pop('is_sorted', False)
    related_name = 'picture_{}'.format(meta.model_name)
    if related_name not in picture_models:
        class_name = 'Picture{}'.format(meta.model_name.capitalize())
        caption_name = 'picture_caption_{}'.format(meta.model_name)
        app_label = meta.app_label
        meta = type('Meta', (), dict(app_label=app_label))

        if is_sorted:
            meta = type('Meta', (),
                        dict(app_label=app_label, ordering=['sort_value']))
            fields['sort_value'] = models.IntegerField('Weight', default=0)

        fields.update(
            dict(
                Meta=meta,
                __module__='{}.models'.format(app_label),
                caption=message_field(caption_name, blank=True, null=True),
                linked_object=models.ForeignKey(
                    model,
                    on_delete=models.CASCADE,
                    related_name=related_name,
                )), )
        picture_models[related_name] = type(class_name, (BasePicture, ),
                                            fields)

    return picture_models[related_name]


class EmbedMedia(models.Model):
    PROVIDERS = (
        ('youtube', 'Youtube'),
        ('vimeo', 'Vimeo'),
        ('roundme', 'RoundMe'),
    )

    id = models.AutoField(primary_key=True)
    tree = models.ForeignKey(to='Tree', on_delete=models.CASCADE)

    provider = models.CharField(
        max_length=12, choices=PROVIDERS, default='youtube')
    media_id = models.CharField(max_length=256)

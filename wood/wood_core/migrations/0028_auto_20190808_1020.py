# Generated by Django 2.1.1 on 2019-08-08 10:20

from django.db import migrations, models
import wood_core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wood_core', '0027_auto_20190802_0935'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contribution',
            name='position',
        ),
        migrations.RemoveField(
            model_name='contribution',
            name='taxon',
        ),
        # migrations.AlterField(
        #     model_name='tree',
        #     name='position',
        #     field=wood_core.fields.PointField(blank=True, null=True, srid=4326),
        # ),
        # migrations.AlterField(
        #     model_name='tree',
        #     name='validated',
        #     field=models.BooleanField(default=False),
        # ),
    ]

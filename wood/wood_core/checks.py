from django.conf import settings
from django.core.checks import Error, register


@register()
def check_public_group(app_configs, **kwargs):
    errors = []
    if getattr(settings, 'STATIC_ROOT', '') is None:
        errors.append(
            Error(
                'No Static Root Configured',
                hint='Set STATIC_ROOT in your settings',
                obj=settings,
                id='wood.E001',
            ))
    return errors

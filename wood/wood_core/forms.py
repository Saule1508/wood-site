from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.forms import (
    ModelForm,
    ModelChoiceField,
    ImageField,
    formset_factory,
)
from django import forms
from django.forms.widgets import ClearableFileInput
from wood_botanique.models.species import Taxon
from wood_lingua.models.message import get_model
from .models.contrib import Contribution, PictureContrib, TextContribution, ImageContribution
from .models.subject import Tree


class BaseForm(ModelForm):

    @property
    def has_instance(self):
        return self.instance.pk is not None


def make_taxon_field(lang):
    class LocalizedTaxonField(ModelChoiceField):
        def label_from_instance(self, obj):
            return getattr(obj.name, lang)

    order_by = 'name__{}'.format(lang)

    return LocalizedTaxonField(
        queryset=Taxon.objects.order_by(order_by), required=False)


# PictureContribFormSet = inlineformset_factory(
#     Contribution,
#     PictureContrib,
#     fk_name='linked_object',
#     fields=(
#         'id',
#         'file',
#         # 'caption',
#     ),
#     extra=4)

text_contrib_fields = (
    'lang',
    # 'taxon',
    'contributor',
    'tree',
    'text',
)
img_contrib_fields = (
    'lang',
    'contributor',
    'tree',
    # 'taxon',
    'picture',
)
# contrib_fields = (
#     'taxon',
#     'position',
#     'description',
#     'image',
# )
tree_fields = (
    'taxon',
    'name',
    'position',
    'description',
    'photomaton',
    'height',
    'circumference',
    'contributor',
)


class TextContribForm(BaseForm):
    # taxon = forms.ModelChoiceField(
    #     queryset=Taxon.objects.all(),
    #     widget=forms.HiddenInput(),
    # )
    tree = forms.ModelChoiceField(
        queryset=Tree.objects.all(),
        widget=forms.HiddenInput(),
    )
    contributor = forms.ModelChoiceField(
        queryset=User.objects.all(),
        widget=forms.HiddenInput(),
    )

    class Meta:
        model = TextContribution
        fields = text_contrib_fields


def text_contrib_form(lang, *args, **kwargs):
    return TextContribForm(*args, **kwargs)


class ImgContribForm(BaseForm):
    # taxon = forms.ModelChoiceField(
    #     queryset=Taxon.objects.all(),
    #     widget=forms.HiddenInput(),
    # )
    tree = forms.ModelChoiceField(
        queryset=Tree.objects.all(),
        widget=forms.HiddenInput(),
    )
    contributor = forms.ModelChoiceField(
        queryset=User.objects.all(),
        widget=forms.HiddenInput(),
    )

    class Meta:
        model = ImageContribution
        fields = img_contrib_fields


def img_contrib_form(lang, *args, **kwargs):
    return ImgContribForm(*args, **kwargs)

ImgContribFormSet = formset_factory(
    ImgContribForm,
    extra=3)

# class ContribFormFr(BaseForm):
#     taxon = make_taxon_field('fr')

#     class Meta:
#         model = Contribution
#         fields = contrib_fields


# class ContribFormNl(BaseForm):
#     taxon = make_taxon_field('nl')

#     class Meta:
#         model = Contribution
#         fields = contrib_fields

# class ContribFormEn(BaseForm):
#     taxon = make_taxon_field('en')

#     class Meta:
#         model = Contribution
#         fields = contrib_fields


# def contrib_form(lang, *args, **kwargs):
#     if 'fr' == lang:
#         return ContribFormFr(*args, **kwargs)
#     elif 'nl' == lang:
#         return ContribFormNl(*args, **kwargs)
#     elif 'en' == lang:
#         return ContribFormEn(*args, **kwargs)


class AddTreeFormFr(BaseForm):
    taxon = make_taxon_field('fr')
    name = forms.ModelChoiceField(
        queryset=get_model('tree_name').objects.all(),
        widget=forms.HiddenInput(),
    )
    description = forms.ModelChoiceField(
        required=True,
        queryset=get_model('tree_description').objects.all(),
        widget=forms.HiddenInput(),
    )
    contributor = forms.ModelChoiceField(
        queryset=User.objects.all(),
        widget=forms.HiddenInput(),
    )
    photomaton = forms.ImageField()

    class Meta:
        model = Tree
        fields = tree_fields


class AddTreeFormNl(BaseForm):
    taxon = make_taxon_field('nl')

    name = forms.ModelChoiceField(
        queryset=get_model('tree_name').objects.all(),
        widget=forms.HiddenInput(),
    )
    description = forms.ModelChoiceField(
        queryset=get_model('tree_description').objects.all(),
        widget=forms.HiddenInput(),
    )
    contributor = forms.ModelChoiceField(
        queryset=User.objects.all(),
        widget=forms.HiddenInput(),
    )
    photomaton = forms.ImageField()

    class Meta:
        model = Tree
        fields = tree_fields


class AddTreeFormEn(BaseForm):
    taxon = make_taxon_field('en')

    name = forms.ModelChoiceField(
        queryset=get_model('tree_name').objects.all(),
        widget=forms.HiddenInput(),
    )
    description = forms.ModelChoiceField(
        queryset=get_model('tree_description').objects.all(),
        widget=forms.HiddenInput(),
    )
    contributor = forms.ModelChoiceField(
        queryset=User.objects.all(),
        widget=forms.HiddenInput(),
    )
    photomaton = forms.ImageField()

    class Meta:
        model = Tree
        fields = tree_fields


def add_tree_form(lang, *args, **kwargs):
    if 'fr' == lang:
        return AddTreeFormFr(*args, **kwargs)
    elif 'nl' == lang:
        return AddTreeFormNl(*args, **kwargs)
    elif 'en' == lang:
        return AddTreeFormEn(*args, **kwargs)

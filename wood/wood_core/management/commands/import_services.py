#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from pathlib import Path
from csv import DictReader
import re
from collections import namedtuple
from uuid import uuid4

from django.core.management.base import BaseCommand, CommandError

from wood_core.models.subject import Tree
from wood_core.models.service import TreeQuality, TreeService
from wood_lingua.models import message, simple_message


def import_id(row):
    return row['ID']


# KKK (nuttige soort voor een hele reeks insecten en vogels)
extractKKtruc_re = re.compile(r"^(.+) \(.+")


def get_quality_score(s):
    # print('getscore {}, {},{}'.format(this, options, s))
    score = 0
    m = re.match(extractKKtruc_re, s)
    if m is None:
        return score

    g, = m.groups()
    c = g[0]
    if 'K' == c:
        score = len(g)

    elif '/' == c:
        score = -1 * len(g)

    return score


extractKKbidule_re = re.compile(r".+ \((.+)\)")


def get_quality_label(s):
    # print('get_quality_label({})'.format(s))
    Klabel = 'ø'
    m = re.match(extractKKbidule_re, s)
    if m is None:
        return Klabel

    g, = m.groups()
    return g


def create_quality(quantity, fr, nl, en):
    return TreeQuality.objects.create(
        quantity=quantity,
        value=message('tree_quality', fr, nl, en),
    )


def beauty(row):
    k_fr = 'Embellit le paysage'
    k_nl = 'Verfraait het landschap'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def biodiversity(row):
    k_fr = 'Enrichit la biodiversité'
    k_nl = 'Verrijkt de biodiversiteit'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def oxygeny(row):
    k_fr = 'Fournit de l\'oxygène'
    k_nl = 'Levert zuurstof'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def purify(row):
    k_fr = 'Purifie l\'air'
    k_nl = 'Zuivert de lucht'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def filtery(row):
    k_fr = 'Filtre l\'eau'
    k_nl = 'Filtert het water'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def floody(row):
    k_fr = 'Evite les inondations'
    k_nl = 'Voorkomt overstromingen'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def carbony(row):
    k_fr = 'Stocke le carbone'
    k_nl = 'Slaat koolstof op'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def sweety(row):
    k_fr = 'Adoucit le climat'
    k_nl = 'Verzacht het klimaat'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def soily(row):
    k_fr = 'Limite l\'érosion du sol'
    k_nl = 'Beperkt de erosie van de bodem'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def healthy(row):
    k_fr = 'Fait du bien, est utile'
    k_nl = 'Doet goed, is nuttig'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


class Command(BaseCommand):
    help = """Import tree services
"""

    output_transaction = True

    def add_arguments(self, parser):
        parser.add_argument('db_path')

    def handle(self, *args, **options):
        TreeService.objects.all().delete()
        db_path = Path(options['db_path'])

        with open(db_path.as_posix()) as f:
            for row in DictReader(f):
                self.process_row(row)

    def process_row(self, row):
        iid = import_id(row)
        try:
            tree = Tree.objects.get(import_id=iid)

            service = TreeService.objects.create(
                beauty=beauty(row),
                biodiversity=biodiversity(row),
                oxygeny=oxygeny(row),
                purify=purify(row),
                filtery=filtery(row),
                floody=floody(row),
                carbony=carbony(row),
                sweety=sweety(row),
                soily=soily(row),
                healthy=healthy(row),
            )

            tree.service = service
            tree.save()

            self.stdout.write('Created services for ' +
                              self.style.SUCCESS(str(tree.name)))

        except Exception as ex:
            self.stderr.write('Failed for {}:\n{}'.format(iid, ex))

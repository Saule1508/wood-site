#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from pathlib import Path
from csv import DictReader
import re
from collections import namedtuple
import networkx as nx
from uuid import uuid4

from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import GEOSGeometry

from wood_botanique.models.species import Taxon
from wood_core.models.subject import Tree, TreePicture, Jumelage, Activity, LinkedMedia, Remarqueur
from wood_core.models.ancillary import Portrait
from wood_core.models.media import EmbedMedia
from wood_lingua.models import message, simple_message


def norm_ID(bad_id):
    return int(bad_id[3:])


def pad(in_, nb, f='0'):
    s = str(in_)
    return s.rjust(nb, f)


def graph_id(nid):
    return 'ID{}'.format(pad(nid, 6))


def txt_id(nid):
    return 'ID{}'.format(pad(nid, 6))


def compute_desc_fr(root_dir, rid):
    return root_dir.joinpath('fichiersTXT', '{}-Description-FR.txt'.format(
        txt_id(rid))).as_posix()


def compute_desc_nl(root_dir, rid):
    return root_dir.joinpath('fichiersTXT', '{}-Description-NL.txt'.format(
        txt_id(rid))).as_posix()


def latin(row):
    return row['Latin']


def taxon(row):
    return Taxon.objects.find(latin(row))


def import_id(row):
    return row['ID']


def category(row):
    cat = row['Catégorie'].lower()
    if 'remarquable' == cat:
        return Tree.REMARQUABLE
    return Tree.REMARQUE


def name(row):
    return message(
        'tree_name', fr=row['Nom FR'], nl=row['Nom NL'], en=row['Nom EN'])


def read_description(path):
    try:
        with open(path) as f:
            return f.read()
    except Exception:
        return 'could not read the content of {}'.format(path.as_posix())


def description(row, root):
    rid = norm_ID(row['ID'])
    path_fr = compute_desc_fr(
        root,
        rid)  #root.joinpath('', row['Traits / Caractère de l\'indidivu'])
    path_nl = compute_desc_nl(root,
                              rid)  #row['Kenmerken/Karakter van het individu']

    return message(
        'tree_description',
        fr=read_description(path_fr),
        nl=read_description(path_nl),
        en='',
    )


def street_address(row):
    return message(
        'tree_street_address', fr=row['Adresse 1'], nl=row['Adres  1'], en='')


def city(row):
    return message('tree_city', fr=row['Adresse 2'], nl=row['Adres 2'], en='')


def post_code(row):
    return message(
        'tree_post_code', fr=row['Code Postal'], nl=row['Code Postal'], en='')


def position(row):
    x = row['Gps2']
    y = row['Gps1']
    p = GEOSGeometry('POINT({} {})'.format(x, y))
    return p


def height(row):
    return message('tree_height', fr=row['Hauteur'], nl=row['Hoogte'], en='')


def circumference(row):
    return message(
        'tree_circumference',
        fr=row['Circonférence du tronc'],
        nl=row['Omtrek van de stam'],
        en='')


def crown_diameter(row):
    return message(
        'tree_crown_diameter',
        fr=row['Diamètre couronne'],
        nl=row['Diameter kruin'],
        en='')


## << graph


def get_objects(node, pred):
    ks = node.keys()
    results = []
    for k in ks:
        np = node[k]['p']
        if np == pred:
            results.append(val(k))

    return results


def get_first_object(node, pred, default=''):
    results = get_objects(node, pred)
    if len(results) > 0:
        return results[0]

    return default


Obj = namedtuple('Obj', ['id', 'value'])


def obj(val):
    return Obj(uuid4(), val)


def val(o):
    return o.value


## graph >>


def listmedias(root_dir, dir_name):
    filenames = filter(
        lambda x: x.is_dir() is False,
        map(Path,
            root_dir.joinpath('Database-Media', dir_name).glob('*')))
    return sorted(filenames)


photomatonre = re.compile(r".+PhotoMaton01.+")


def is_photomaton(img_name):
    return re.match(photomatonre, img_name) is not None


def get_media_caption(G, name):
    try:
        caption_nl = get_first_object(G[name], 'caption nl')
        caption_fr = get_first_object(G[name], 'caption fr')

        return message(
            'picture_caption_tree',
            fr=caption_fr,
            nl=caption_nl,
            en='',
        )
    except Exception:
        return message('picture_caption_tree', fr='', nl='', en='')


def get_medias(G, root_dir, tree):
    clean_id = norm_ID(tree.import_id)
    dir_name = pad(clean_id, 6)
    filenames = listmedias(root_dir, dir_name)
    for i, f in enumerate(filenames):
        if not is_photomaton(f.name):
            p = TreePicture()
            p.linked_object = tree
            p.sort_value = i * 10
            p.caption = get_media_caption(G, f.name)
            p.file = 'wood_core.tree/{}/{}'.format(dir_name, f.name)
            p.save()


def get_photomaton(root_dir, import_id):
    clean_id = norm_ID(import_id)
    dir_name = pad(clean_id, 6)
    for f in listmedias(root_dir, dir_name):
        if is_photomaton(f.name):
            return 'wood_core.tree/{}/{}'.format(dir_name, f.name)
    return 'wood_core.tree/not-found.png'


def patrimoine(G, import_id):
    gid = graph_id(norm_ID(import_id))
    try:
        node = G[gid]
        url = get_first_object(node, 'lien patrimoine regional fr')
        return int(url.split('=')[-1])
    except Exception:
        return None


def jumelage(G, tree):
    gid = graph_id(norm_ID(tree.import_id))
    for scope in ('belgique', 'europe', 'monde'):
        try:
            node = G[gid]
            urls = get_objects(node, 'lien jumelage {}'.format(scope))
            for url in urls:
                label_fr = get_first_object(G[url], 'label url fr')
                label_nl = get_first_object(G[url], 'label url nl')

                if label_fr is '':
                    label_fr = url

                if label_nl is '':
                    label_nl = url

                label = message(
                    'jumelage_label', fr=label_fr, nl=label_nl, en='')
                Jumelage.objects.create(
                    tree=tree,
                    scope=scope,
                    url=url,
                    label=label,
                )
        except Exception as ex:
            print('Error jumelage: {}'.format(ex))


def linked_medias(G, tree):
    gid = graph_id(norm_ID(tree.import_id))
    try:
        node = G[gid]
        urls = get_objects(node, 'lien media')
        for url in urls:
            label_fr = get_first_object(G[url], 'label url fr')
            label_nl = get_first_object(G[url], 'label url nl')

            if label_fr is '':
                label_fr = url

            if label_nl is '':
                label_nl = url

            LinkedMedia.objects.create(
                tree=tree,
                url=url,
                label=message(
                    'linked_media_label',
                    fr=label_fr,
                    nl=label_nl,
                    en='',
                ))
    except Exception:
        pass


# def get_activite(G, tree):
#     gid = graph_id(norm_ID(tree.import_id))
#     try:
#         node = G[gid]
#         results_fr = []
#         urls_fr = get_objects(node, 'activite url fr')
#         for url in urls_fr:
#             try:
#                 label = get_first_object(G[url], 'label url fr', url)
#                 results_fr.append(dict(url=url, label=label))
#             except KeyError:
#                 results_fr.append(
#                     dict(url=url, label=url_default_label(url)))

#         results_nl = []
#         urls_nl = get_objects(node, 'activite url nl')
#         for url in urls_nl:
#             try:
#                 label = get_first_object(G[url], 'label url nl', url)
#                 results_nl.append(dict(url=url, label=label))
#             except KeyError:
#                 results_nl.append(
#                     dict(url=url, label=url_default_label(url)))

#         return dict(ACTIVITE_FR=results_fr, ACTIVITE_NL=results_nl)

#     except Exception:
#         return dict()


def remarqueur(G, tree):
    gid = graph_id(norm_ID(tree.import_id))
    try:
        node = G[gid]
        name_fr = get_first_object(node, 'remarqueur name fr', None)
        name_nl = get_first_object(node, 'remarqueur name nl', None)
        if (name_fr is None) or (name_nl is None):
            raise Exception('Remarqueur needs a name!')
        url_fr = get_first_object(node, 'remarqueur url fr')
        url_nl = get_first_object(node, 'remarqueur url nl')

        Remarqueur.objects.create(
            tree=tree,
            url=message('remarqueur_url', fr=url_fr, nl=url_nl, en=''),
            label=message('remarqueur_label', fr=name_fr, nl=name_nl, en=''),
        )
    except Exception as ex:
        print('Failed remarqueur {}'.format(ex))


def portrait(G, root_dir, tree):
    gid = graph_id(norm_ID(tree.import_id))
    pdir = root_dir.joinpath('portrait')
    try:
        node = G[gid]
        fr = pdir.joinpath(get_first_object(node, 'portrait fr'))
        nl = pdir.joinpath(get_first_object(node, 'portrait nl'))
        portrait_fr = None
        portrait_nl = None
        with open(fr.as_posix()) as f:
            portrait_fr = f.read()
        with open(nl.as_posix()) as f:
            portrait_nl = f.read()
        if (portrait_fr is not None) and (portrait_nl is not None):
            Portrait.objects.create(
                tree=tree,
                monthly=False,
                body=message('portrait_body', portrait_fr, portrait_nl, ''))
    except Exception:
        pass


def embed_media(G, tree):
    gid = graph_id(norm_ID(tree.import_id))
    for provider, _ in EmbedMedia.PROVIDERS:
        try:
            node = G[gid]
            embeds = get_objects(node, 'embed media {}'.format(provider))
            for media_id in embeds:
                EmbedMedia.objects.create(
                    tree=tree,
                    provider=provider,
                    media_id=media_id,
                )
        except Exception:
            pass


def get_ancillaries(G, root, tree):
    jumelage(G, tree)
    if Tree.REMARQUE == tree.category:
        remarqueur(G, tree)
    portrait(G, root, tree)
    linked_medias(G, tree)
    embed_media(G, tree)


class Command(BaseCommand):
    help = """Import trees
    """

    output_transaction = True
    dones = dict()

    def add_arguments(self, parser):
        parser.add_argument('root')

    def handle(self, *args, **options):

        Tree.objects.all().delete()
        TreePicture.objects.all().delete()
        Jumelage.objects.all().delete()
        Activity.objects.all().delete()
        LinkedMedia.objects.all().delete()
        EmbedMedia.objects.all().delete()
        Remarqueur.objects.all().delete()
        Portrait.objects.all().delete()

        self.root = Path(options['root'])
        db_path = self.root.joinpath('table.csv')
        graph_path = self.root.joinpath('graph.csv')
        self.G = nx.Graph()

        with open(graph_path.as_posix()) as f:
            for row in DictReader(f):
                self.G.add_edge(
                    row['SUJET'], obj(row['OBJET']), p=row['PREDICAT'].strip())

        with open(db_path.as_posix()) as f:
            for row in DictReader(f):
                self.process_row(row)

    def process_row(self, row):
        t = Tree.objects.create(
            import_id=import_id(row),
            patrimoine=patrimoine(self.G, import_id(row)),
            category=category(row),
            taxon=taxon(row),
            photomaton=get_photomaton(self.root, import_id(row)),
            name=name(row),
            description=description(row, self.root),
            street_address=street_address(row),
            city=city(row),
            post_code=post_code(row),
            position=position(row),
            height=height(row),
            circumference=circumference(row),
            crown_diameter=crown_diameter(row),
        )

        get_medias(self.G, self.root, t)
        get_ancillaries(self.G, self.root, t)

        self.stdout.write('Created ' + self.style.SUCCESS(t.name))
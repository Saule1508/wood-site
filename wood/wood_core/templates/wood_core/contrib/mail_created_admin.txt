Bonjour Wood Wide Web,

Un arbre (id:{{contrib.id}}) a été ajouté par {{user.username}} et attend votre validation sur 
https://woodwideweb.be/admin/wood_core/tree/{{contrib.id}}/change/

Bonne découverte!
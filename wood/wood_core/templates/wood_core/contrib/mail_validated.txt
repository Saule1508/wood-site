Bonjour {{user.username}},

Votre arbre a été validé. Vous pouvez le voir sur la carte et à cette adresse:
https://woodwideweb.be/fr/atlas/{{contrib.id}}.html
Merci pour votre participation!

Vous pouvez en ajouter d'autres si vous le souhaitez

L'équipe de Wood Wide Web
https://woodwideweb.be/fr

-----

Hallo {{user.username}},

Jouw bijdrage werd gevalideerd.
Je kan je boom zien op de kaart en op dit adres: 
https://woodwideweb.be/nl/atlas/{{contrib.id}}.html

Je kan extra bomen toevoegen als je dat wenst.

Bedankt voor jouw deelname!

Het team van Wood Wide Web
https://woodwideweb.be/nl

-----

Hello {{user.username}},

Your tree has been validated.
You can see it on the map and at this address:
https://woodwideweb.be/en/atlas/{{contrib.id}}.html
Thank you for your participation!

You are welcome to add more if you want.

Wood Wide Web's team
https://woodwideweb.be/en
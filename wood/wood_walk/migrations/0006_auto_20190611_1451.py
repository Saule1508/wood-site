# Generated by Django 2.1.1 on 2019-06-11 14:51

import django.contrib.gis.db.models.fields
from django.db import migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wood_walk', '0005_auto_20190606_1435'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='picturewalkedge',
            name='caption',
        ),
        migrations.RemoveField(
            model_name='picturewalkedge',
            name='linked_object',
        ),
        migrations.RemoveField(
            model_name='walkedge',
            name='photomaton',
        ),
        migrations.AddField(
            model_name='walkedge',
            name='image',
            field=easy_thumbnails.fields.ThumbnailerImageField(blank=True, null=True, upload_to='walk_edge/'),
        ),
        migrations.DeleteModel(
            name='PictureWalkedge',
        ),
    ]

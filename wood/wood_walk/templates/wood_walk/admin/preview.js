

(function () {

    const jsonFormat = new OpenLayers.Format.GeoJSON()

    const readJSON = (obj) => jsonFormat.read(JSON.stringify(obj), 'FeatureCollection')

    const myStyles = new OpenLayers.StyleMap({
        "default": new OpenLayers.Style({
            pointRadius: 3,
            fillColor: "#white",
            strokeColor: "#97cfb1",
            strokeWidth: 2,
            graphicZIndex: 1
        }),
        "select": new OpenLayers.Style({
            fillColor: "#66ccff",
            strokeColor: "#3399ff",
            graphicZIndex: 2
        })
    });

    const walkStyles = new OpenLayers.Style({
        strokeColor: "#4CCB7E",
        strokeWidth: 2,
        strokeDashstyle: "dash",
    }, {
            rules: [new OpenLayers.Rule({
                evaluate: (feature) => current_walk === feature.data.id,
            })]
        });

    let trees;
    const makeFeatureFromPoint =
        (p) =>
            new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(p[0], p[1]));

    function makeFeatureFromNode(n) {
        const id = parseInt(n.value, 10)
        let tree = findTree(id);
        if (tree !== undefined) {
            return makeFeatureFromPoint(tree.geometry.coordinates);
        } else {
            console.log('No tree found');
        }
    }

    function findTree(id) {
        const fs = trees.features;
        return (
            fs
                .filter(f => f.properties.category !== 'inventory' && f.properties.category !== 'adopte')
                .find(f => f.properties.id === id)
        )
    }

    //inOrOut = 'in' if in_node, 'out' if out_node
    function setNewPoint(event, map, layer, points, inOrOut) {
        if (inOrOut === 'in') { i = 0 }
        else { i = 1 }
        // console.log('changes happened!')
        if (points[i]) {
            layer.removeFeatures(points[i]);
        }
        new_in_node = event.target;
        points[i] = makeFeatureFromNode(new_in_node);
        console.log('node' + new_in_node + ' point ' + points[0]);
        layer.addFeatures(points[i]);
        layer.redraw();

        // const coords = findTree(parseInt(new_in_node.value)).geometry.coordinates
        if (points[0]) {
            x0 = points[0].geometry.x;
            y0 = points[0].geometry.y;
        }
        if (points[1]) {
            x1 = points[1].geometry.x;
            y1 = points[1].geometry.y;
        }

        var bounds = new OpenLayers.Bounds();
        const bfr = 500;
        if (points[0] && points[1]) {

            if (x1 > x0) {
                if (y1 > y0) {
                    bounds = [x0 - bfr, y0 - bfr, x1 + bfr, y1 + bfr]
                    // bounds = extend(new OpenLayer.LonLat(x0 - bfr, y0 - bfr))
                    // bounds = extend(new OpenLayer.LonLat(x1 + bfr, y1 + bfr))
                    // console.log('case1');
                } else {
                    bounds = [x0 - bfr, y1 - bfr, x1 + bfr, y0 + bfr]
                }
            } else {
                if (y1 > y0) {
                    bounds = [x1 - bfr, y0 - bfr, x0 + bfr, y1 + bfr]
                } else {
                    bounds = [x1 - bfr, y1 - bfr, x0 + bfr, y0 + bfr]
                }
            }
            map.zoomToExtent(bounds, 4);

        } else {
            const coords = findTree(parseInt(new_in_node.value)).geometry.coordinates
            bounds = [coords[0] - bfr, coords[1] - bfr, coords[0] + bfr, coords[1] + bfr]
            // bounds = extend(new OpenLayer.LonLat(coords[0] - bfr, coords[1] - bfr))
            // bounds = extend(new OpenLayer.LonLat(coords[0] + bfr, coords[1] + bfr))
            map.zoomToExtent(bounds, 4);
        }



    }

    let current_walk; //id of the selected walk
    const edges_layer = new OpenLayers.Layer.Vector("Walks", { styleMap: new OpenLayers.StyleMap({ default: walkStyles }) });

    function start() {
        let in_node = document.getElementById('id_in_node')
        let out_node = document.getElementById('id_out_node')
        let map = geodjango_path.map;
        let points = [];

        // make a layer for node points
        let l = new OpenLayers.Layer.Vector("Points", {
            styleMap: myStyles,
            rendererOptions: { zIndexing: true }
        });

        let tree_in = findTree(parseInt(in_node.value));
        if (tree_in !== undefined) {
            p1 = makeFeatureFromPoint(tree_in.geometry.coordinates)
            points.push(p1)
        };
        let tree_out = findTree(parseInt(out_node.value));
        if (tree_out !== undefined) {
            p2 = makeFeatureFromPoint(tree_out.geometry.coordinates)
            points.push(p2)
        };

        l.addFeatures(points);
        map.addLayer(l);

        //if no nodes at begining or nodes changes
        in_node.addEventListener('change', e => setNewPoint(e, map, l, points, 'in'));
        out_node.addEventListener('change', e => setNewPoint(e, map, l, points, 'out'));


        // --- Add selected walk to the map ---

        const walk_element = document.getElementById('id_walk');
        current_walk = parseInt(walk_element.value)
        walk_element.addEventListener('change', (e) => {
            current_walk = parseInt(e.target.value);
            // edges_layer.refresh({ force: true });
            edges_layer.redraw();
        });

        map.addLayer(edges_layer);
    }

    document.onreadystatechange = function startApplication() {
        if ('interactive' === document.readyState) {
            fetch('/atlas/data/')
                .then(response => response.json())
                .then(gd => trees = gd)
                .then(() => {
                    fetch('/walk/paths')
                        .then(response => response.json())
                        .then(fc => {
                            const OLfeatures = readJSON(fc)
                            edges_layer.addFeatures(OLfeatures)
                        })
                })
                .then(start);
        }
    };
})();
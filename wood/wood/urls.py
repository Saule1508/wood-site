"""wood URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.http import HttpResponseNotFound
from django.contrib.auth import views as auth_views
from .views import WoodLogin, WoodRegister, WoodLogout
from django.views.generic.base import TemplateView
from django.http import HttpResponse

from django.views.generic.base import RedirectView


admin.site.site_header = 'wood wide web'
admin.site.site_title = 'wood wide web'
admin.site.index_title = 'Admin'

# def fav(request):
#     return HttpResponseNotFound('no-ico')

urlpatterns = [
    # path('favicon.ico', fav),
    path('favicon.ico', RedirectView.as_view(url='wood_core/static/wood_core/svg/favicon.png')),
    path('robots.txt', lambda x: HttpResponse("User-Agent: *\nDisallow:", content_type="text/plain"), name="robots_file"),

    path('admin/', admin.site.urls),
    
    # path('accounts/', include('django_registration.backends.one_step.urls')),
    path('<lang>/accounts/registration/', WoodRegister.as_view(), name='registration'),

    path('register/closed/',
        TemplateView.as_view(
            template_name='django_registration/registration_closed.html'
        ),
        name='django_registration_disallowed'),
    path('register/complete/',
        TemplateView.as_view(
            template_name='django_registration/registration_complete.html'
        ),
        name='django_registration_complete'),


    path('<lang>/accounts/login/', WoodLogin.as_view(), name='login'),
    path('<lang>/accounts/logout/', WoodLogout.as_view(), name='logout'),

    path('password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),

    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),


    path('', include('wood_core.urls')),
    path('', include('wood_walk.urls')),

    path('lingua/', include('wood_lingua.urls')),
]
